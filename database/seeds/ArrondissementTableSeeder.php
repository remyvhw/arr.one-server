<?php

use Illuminate\Database\Seeder;

class ArrondissementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

    	DB::table('arrondissements')->insert([
            'name' => "Le Plateau-Mont-Royal",
            'rss'  => "https://webtv.coop/rss/group/df10328ccbbb850fdf31d7b775cdc2ab",
            "image" => "Plateau",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Rosemont–La Petite-Patrie",
            'rss'  => "https://webtv.coop/rss/group/7518f7e3e2e15f10c3dd9f2a2f9fce1e",
            "image" => "Rosemont",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Rivière-des-Prairies–Pointe-aux-Trembles",
            'rss'  => "https://webtv.coop/rss/group/4cc85496bbb3737682545b67a6758378",
            "image" => "Pat",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Outremont",
            'rss'  => "https://webtv.coop/rss/group/53d0c3cae399ddf5166c093ea1f97acd",
            "image" => "Outremont",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Pierrefonds-Roxboro",
            'rss'  => "https://webtv.coop/rss/group/e38e339f298230b13154d02b21e4a9cd",
            "image" => "Pierrefonds",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Verdun",
            'rss'  => "https://webtv.coop/rss/group/382e088bccdfb441e705e2e64420234c",
            "image" => "Verdun",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Mercier–Hochelaga-Maisonneuve",
            'rss'  => "https://webtv.coop/rss/group/3e190cb69d1fc6bcb533e6e351dea5d0",
            "image" => "Hochelaga",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Villeray–Saint-Michel–Parc-Extension",
            'rss'  => "https://webtv.coop/rss/group/df99b34bba5a8188aeea4f40b3027415",
            "image" => "Vsmpe",
        ]);

		DB::table('arrondissements')->insert([
            'name' => "Le Sud-Ouest",
            'rss'  => "https://webtv.coop/rss/group/99c262c1176229ad377dc0834a5729f3",
            "image" => "Sudouest",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Ahuntsic-Cartierville",
            'rss'  => "https://webtv.coop/rss/group/4cf7ea9405ceacb24872b9608e699729",
            "image" => "Ahuntsic",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Montréal-Nord",
            'rss'  => "https://webtv.coop/rss/group/295f57a2dde04f992a3932374dcdf2b6",
            "image" => "Mtlnord",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Lachine",
            'rss'  => "https://webtv.coop/rss/group/155ceedadf1258039c0b4ed21bb2339d",
            "image" => "Lachine",
        ]);

        DB::table('arrondissements')->insert([
            'name' => "Ville-Marie",
            'rss'  => "https://webtv.coop/rss/group/6ea619554e30d1600a43968c7cd2e5d5",
            "image" => "Villemarie",
            
        ]);
        
    }
}
