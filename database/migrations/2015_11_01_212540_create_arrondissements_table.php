<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArrondissementsTable extends Migration {

	public function up()
	{
		Schema::create('arrondissements', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 256);
			$table->string("rss", 1024)->nullable();
			$table->string('image', 256);
		});
	}

	public function down()
	{
		Schema::drop('arrondissements');
	}
}