<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrondissement extends Model {

	protected $table = 'arrondissements';
	public $timestamps = false;

}