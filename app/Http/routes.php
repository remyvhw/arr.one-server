<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

 Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]], function()
    {
  	
  	Route::get('/', function () {
    	return view('welcome');
	});

	Route::group(['prefix' => "tvml"], function () {
		Route::get('js/application.js', "TvmlController@getApplication");
		Route::get('js/Presenter.js', "TvmlController@getPresenter");
		Route::get('js/ResourceLoader.js', "TvmlController@getResourceLoader");
		Route::get('templates/Index.xml.js', "TvmlController@getIndex");
		Route::get('templates/borough/{id}/Index.xml.js', "TvmlController@getBorough");
		Route::get('templates/council/{url}', "TvmlController@getCouncil");
		Route::get('templates/vcentre/vdm/Index.xml.js', "TvmlController@getCityCouncil");
		
	});

});
