<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TvmlController extends Controller
{
    protected function cleanFeedText($text){

        $text = str_replace("&eacute;", "é", $text);
        $text = str_replace("&amp;#039;", "'", $text);
        $text = htmlspecialchars($text, ENT_XML1, 'UTF-8');
        $text = str_replace("&amp;ndash;", "-", $text);
        $text = str_replace("&amp;rsquo;", "'", $text);
        $text = str_replace("&amp;ucirc;", "û", $text);

        return $text;
    }

    public function getApplication()
    {
        //
        return response()->view('tvml.application_js')->header('Content-Type', 'application/javascript');
    }

    public function getPresenter()
    {
        //
        return response()->view('tvml.Presenter_js')->header('Content-Type', 'application/javascript');
    }
    
    public function getResourceLoader(){
        return response()->view('tvml.RessourceLoader_js')->header('Content-Type', 'application/javascript');

    }

    public function getIndex(){
        $arrondissements = \App\Arrondissement::whereNotNull('rss')->orderBy('name', 'asc')->get();

        return response()->view('tvml.Index_xml_js', compact("arrondissements"))->header('Content-Type', 'application/javascript');

    }

    public function getBorough($id){

        $arrondissement = \App\Arrondissement::findOrFail($id);

        $rss = \FeedReader::read($arrondissement->rss);

        foreach ($rss->get_items() as $item){
            $title = $this->cleanFeedText($item->get_title());

            $titles = preg_split( "/ (-|du) /", $title );

            if (count($titles) >1 ){
                $item->xmltitle = $titles[0];
                unset($titles[0]);
                $subtitle = implode(" - ", $titles);
                $subtitle = str_replace("Plateau Mont-Royal, ", "", $subtitle);
                $item->xmlsubtitle = $subtitle;

            } else {
                $item->xmltitle = $title;
                $item->xmlsubtitle = null;   
            }

            if (($enclosure = $item->get_enclosure()) && ($thumbnail = $enclosure->get_thumbnail())){
                $item->xmlthumbnail = $thumbnail;
            } else {
                $item->xmlthumbnail = null;
            }

            $cleaned_up_url = str_replace("www.", "", $item->get_permalink());
            $cleaned_up_url = str_replace("http://webtv.coop/", "", $cleaned_up_url);
            $cleaned_up_url = str_replace("https://webtv.coop/", "", $cleaned_up_url);
            $item->xmllink = urlencode(base64_encode($cleaned_up_url));
            
            $items[] = $item;
        }

        return response()->view('tvml.Borough_xml_js', compact("arrondissement", "items"))->header('Content-Type', 'application/javascript');

    }


    public function getCouncil($url){

        $decoded_url = "https://webtv.coop/" . base64_decode($url);
        $source = file_get_contents($decoded_url);
        $re1='.*?'; # Non-greedy match on filler
        $re2='((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s"]*))';   # HTTP URL 1

        if ($c=preg_match_all ("/".$re1.$re2."\.m3u8/is", $source, $matches)) {
            $httpurl1=$matches[1][0];
            $hls_url = $httpurl1 . ".m3u8";
            return response()->json(compact("hls_url"));
        }
        abort(400);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCityCouncil()
    {
        //
        $rss = \FeedReader::read("https://webtv.coop/rss/group/df10328ccaaa850fdf31d7b775cdc2ab");

        foreach ($rss->get_items() as $item){
            $title = $this->cleanFeedText($item->get_title());
            $title = str_replace("Assemblée du ", "", $title);
            $title = str_replace(", ", " - ", $title);
            $title = str_replace(" de la Ville de Montréal", "", $title);
            $title = str_replace("comité", "Comité", $title);
            $title = str_replace("conseil municipal de Montréal", "Conseil municipal", $title);
            

            $titles = preg_split( "/ (-|du) /", $title );

            if (count($titles) >1 ){
                $item->xmltitle = $titles[0];
                unset($titles[0]);
                $subtitle = implode(" - ", $titles);
                $item->xmlsubtitle = $subtitle;

            } else {
                $item->xmltitle = $title;
                $item->xmlsubtitle = null;   
            }

            if (($enclosure = $item->get_enclosure()) && ($thumbnail = $enclosure->get_thumbnail())){
                $item->xmlthumbnail = $thumbnail;
            } else {
                $item->xmlthumbnail = null;
            }

            $cleaned_up_url = str_replace("www.", "", $item->get_permalink());
            $cleaned_up_url = str_replace("http://webtv.coop/", "", $cleaned_up_url);
            $cleaned_up_url = str_replace("https://webtv.coop/", "", $cleaned_up_url);
            $item->xmllink = urlencode(base64_encode($cleaned_up_url));
            

            $items[] = $item;
        }

        return response()->view('tvml.Vcentre_xml_js', compact("items"))->header('Content-Type', 'application/javascript');

    }
    
    
    
}
