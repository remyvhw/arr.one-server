<!DOCTYPE html>
<html>
    <head>
        <title>arr.one</title>

<link rel="stylesheet" type="text/css" class="ui" href="https://oss.maxcdn.com/semantic-ui/2.1.6/semantic.min.css">
<script src="https://use.typekit.net/ghx0uwe.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/favicon.png">

<style type="text/css">
    body {
      background-color: #911101;
      background-image: url('/montreal.svg');
      background-size:100% 100%;
      background-size:cover;
    }
    body > .grid {
      height: 100%;
    }
    .header {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
    .header .content{
         font-family: "clarendon-text-pro-n7", "clarendon-text-pro",sans-serif;
         font-size: 2.7em;
         font-weight: 700;
         font-style: normal;
         color: #fff;
    }
  </style>
    </head>
    <body>
        

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui header">
      <div class="content">
        arr.one
      </div>
    </h2>
    

    <div class="ui message">
      <p>@lang("arrone.welcome")</p>
      <a href="mailto:support@arr.one" tabindex="0" class="ui basic button"><i class="icon envelope"></i>
       @lang("arrone.contact")</a>

      <div class="ui divider"></div>
      <p>@lang("arrone.opensource")</p>

      <a href="https://bitbucket.org/remyvhw/arr.one-server" tabindex="0" class="ui basic small button"><i class="icon bitbucket"></i>
       @lang("arrone.downloadsource")</a>
    </div>
  </div>
</div>




    </body>
</html>
