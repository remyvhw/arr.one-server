var Template = function() { return `<?xml version="1.0" encoding="UTF-8" ?>
<document>
  <head>
  </head>
  <listTemplate>
    <list>
      <relatedContent>
        <itemBanner>
          <heroImg src="${this.BASEURL}resources/images/italy/italy_10_square.jpg" />
        </itemBanner>
      </relatedContent>
      <header>
        <title>@lang("arrone.citycouncil")</title>
      </header>
      <section>

        @foreach ($items as $item)
        <listItemLockup data-video="{{ URL::to('/tvml/templates/council/' . $item->xmllink) }}">
          @if ($item->xmlthumbnail)
          <img src="{{ $item->xmlthumbnail }}" width="199" height="109" />
          @endif
          <title>{!! $item->xmltitle !!}</title>
          @if ($item->xmlsubtitle)
          <subtitle>{!! $item->xmlsubtitle !!}</subtitle>
          @endif
          <relatedContent>
      
         <itemBanner>
          <heroImg src="{{ $item->xmlthumbnail }}" />
          <title>{!! $item->xmltitle !!}</title>
          <subtitle>{!! $item->xmlsubtitle !!}</subtitle>
        </itemBanner>

         </relatedContent>
        </listItemLockup>
        @endforeach

      </section>
    </list>
  </listTemplate>
</document>`
}
