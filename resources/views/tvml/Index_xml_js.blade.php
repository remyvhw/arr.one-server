var Template = function() { return `<?xml version="1.0" encoding="UTF-8" ?>
  <document>
    <head>
    </head>
    <catalogTemplate>
      <banner>
        <heroImg src="{{ URL::to('/logotype-tvml-index.png') }}" width="1920" height="144" />
      </banner>
      <list>
        <section>
          <header>
            <title>@lang("arrone.content")</title>
          </header>

          <listItemLockup>
            <title>@lang("arrone.boroughs")</title>
            <decorationLabel>{{ count($arrondissements) }}</decorationLabel>
            <relatedContent>
              <grid>
                <section>
                @foreach ($arrondissements as $arrondissement)
                  <lockup template="{{ URL::to('/tvml/templates/borough/' . $arrondissement->id . '/Index.xml.js') }}">
                    <img src="{{ asset('arrondissements/square/' . $arrondissement->image . '.lsr') }}" width="308" height="308" />
                    <title class="whiteText">{!! $arrondissement->name !!}</title>
                  </lockup>
                @endforeach
                </section>
              </grid>
            </relatedContent>
          </listItemLockup>

          <listItemLockup>
            <title>@lang("arrone.city")</title>
            <relatedContent>
              <grid>
                <section>
                  <lockup template="{{ URL::to('/tvml/templates/vcentre/vdm/Index.xml.js') }}">
                    <img src="{{ asset('vcentre/square/executif.lsr') }}" width="308" height="308" />
                    <title class="whiteText">@lang("arrone.citycouncil")</title>
                  </lockup>
                </section>
              </grid>
            </relatedContent>
          </listItemLockup>

        </section>
      </list>
    </catalogTemplate>
  </document>`
}
