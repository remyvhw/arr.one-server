<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'content' => "Contenu",
    'boroughs' => 'Arrondissements',
    "citycouncil"=>"Conseil de ville",
    "city"=>"Ville",
    "welcome"=>"La politique municipale est indéniablement un sujet excitant; arr.one est une application pour Apple TV qui vise à vous offrir de quoi occuper vos samedis soirs en vous donnant un accès VIP à l'endroit où l'action se passe à Montréal: le conseil de ville. arr.one est non-officiel, extrèmement limité en fonctionalités et <em>un tantinet</em> pas catholique (en terme de programmation) et sans garanties.",
    "opensource"=>"Sentez vous libre de télécharger le code source du serveur de arr.one (le client utilise grosso modo le code exemple d'Apple) pour aborder vos propres projets de développement sur Apple TV (avec TVMLKit).",
    "downloadsource"=>"Voir sur Bitbucket",
    "loading"=>"Chargement",
    "contact"=>"Contact",
];
