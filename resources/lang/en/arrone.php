<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'content' => "Content",
    'boroughs' => 'Boroughs',
    "citycouncil"=>"City Council",
    "city"=>"City",
    "welcome"=>"Municipal politics can be exciting; arr.one is an Apple TV app built to help you spend your Saturday night right where the action is: city council. arr.one is unofficial, highly limited in functionalities and <em>slightly</em> hackish.",
    "opensource"=>"Feel free to download the source code and use it as a starting point for your own Apple TV (Client-Server TVMLKit) projects.",
    "downloadsource"=>"See on Bitbucket",
    "loading"=>"Loading",
    "contact"=>"Contact",
];
