## arr.one

Municipal politics can be exciting; arr.one is an Apple TV app built to help you spend your Saturday night right where the action is: city council. arr.one is unofficial, highly limited in functionalities and *slightly* hackish.


### License

arr.one code is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT). Please note that the content itself (the Borough's names, thumbnails and videos) is **not** covered by this license.
